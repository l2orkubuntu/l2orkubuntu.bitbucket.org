# Ubuntu PPAs for Pd-L2Ork and Friends

**NOTE:** As of August 2019, I'm maintaining all these packages at the [Open Build Service](https://build.opensuse.org/) now. Please head over to https://build.opensuse.org/project/show/home:aggraef to find the new repositories.The following wiki page will tell you how to install from the OBS repositories: https://github.com/agraef/purr-data/wiki/Installation

The Ubuntu PPAs will stick around for the time being, but won't be updated any longer.

Permanent link to this page for bookmarking: https://l2orkubuntu.bitbucket.io

These PPAs provide a build of Pd-L2Ork and corresponding Pure and Faust plugins, as well as all required dependencies not readily available in the standard Ubuntu repositories. The PPAs are all hosted on [Launchpad](https://launchpad.net/~dr-graef).

Pd-L2Ork is the [Linux Laptop Orchestra](http://l2ork.music.vt.edu/) (L2Ork) version of [Pure Data](http://puredata.info/) (also known as Pd), [Miller Puckette's](http://msp.ucsd.edu/) real-time graphical programming environment for audio and graphics processing. Pd-L2Ork was originally created by [Ivica Ico Bukvic](http://www.performingarts.vt.edu/faculty-staff/view/ivica-ico-bukvic) from the School of Performing Arts at Virginia Tech. It is based on [Hans-Christoph Steiner's](http://at.or.at/hans/) popular (but no longer maintained) [Pd-extended](http://puredata.info/download/pd-extended) distribution of Pd, but includes many additional bugfixes and user interface enhancements.

All PPAs offer Jonathan Wilkes' new [nw.js](http://nwjs.io/)-based Pd-L2Ork 2.x version, nick-named [Purr Data](https://agraef.github.io/purr-data/). This is a cross-platform version of Pd-L2Ork with an improved JavaScript GUI.

For the time being, Bukvic's original [Pd-L2Ork](http://l2ork.music.vt.edu/main/make-your-own-l2ork/software/) (Pd-L2Ork 1.x a.k.a. "classic Pd-L2Ork") is still available as well, and can be installed alongside the new 2.x version.

The repositories also provide Pd-Pure and Pd-Faust plugins for use with Pd-L2Ork/Purr-Data. Using these plugins you can extend Pd-L2Ork with externals written in the [Pure](http://purelang.bitbucket.org/) and [Faust](http://faust.grame.fr/) programming languages.

## Reporting Bugs

Out-of-date packages and other packaging problems can be reported either directly on [Launchpad](https://launchpad.net/~dr-graef), or you can use the [issue tracker](https://bitbucket.org/l2orkubuntu/l2orkubuntu.bitbucket.org/issues) on this page.

## Available PPAs

We generally keep packages for different Ubuntu versions in different PPAs, as Launchpad eases the maintenance of that kind of setup. At present, we maintain packages for the last two LTSes, Ubuntu 18.04 (Bionic) and 16.04 (Xenial), as well as the latest non-LTS version (at the time of this writing, Disco a.k.a. Ubuntu 19.04):

- https://launchpad.net/~dr-graef/+archive/ubuntu/pd-l2ork.disco
- https://launchpad.net/~dr-graef/+archive/ubuntu/pd-l2ork.bionic
- https://launchpad.net/~dr-graef/+archive/ubuntu/pd-l2ork.xenial

Note that if you want to also install the pd-pure and pd-faust packages, then you'll need some dependencies provided in the corresponding Pure PPAs, which can be found here:

- https://launchpad.net/~dr-graef/+archive/ubuntu/pure-lang.disco
- https://launchpad.net/~dr-graef/+archive/ubuntu/pure-lang.bionic
- https://launchpad.net/~dr-graef/+archive/ubuntu/pure-lang.xenial

**NOTE:** If you need packages for any other Ubuntu versions > 16.04 then please just let us know by submitting an [issue](https://bitbucket.org/l2orkubuntu/l2orkubuntu.bitbucket.org/issues). Please note, however, that we can only provide packages for recent Ubuntu releases which are still supported on Launchpad.

NB: For the time being, packages for Ubuntu Trusty (14.04) are still available as well, and we'll probably keep these around as long as Purr Data still builds there, but these may go away at any time.

## Usage

Please follow the links above for detailed instructions. In brief, use the following commands to add the PPAs to your system (shown examples are for Bionic, if you're using a different Ubuntu version then you need to change the suffix of the PPA name accordingly):

```
sudo add-apt-repository ppa:dr-graef/pd-l2ork.bionic
sudo add-apt-repository ppa:dr-graef/pure.bionic
sudo apt-get update
```

Enter your admin password when prompted. The second command can be omitted if you do not plan to install pd-pure or pd-faust. Then run the `apt-get install` command to install the packages that you want/need, e.g.:

```
sudo apt-get install purr-data
```

To remove this package again:

```
sudo apt-get remove purr-data
```

Of course, instead of apt-get you can also use a graphical package manager such as [synaptic](https://help.ubuntu.com/community/SynapticHowto).

## Alternative and Upstream Packages

- We also provide basically the same set of packages for Arch, you can find these [here](https://l2orkaur.bitbucket.io/).

- Virginia Tech's official pd-l2ork packages are available at http://l2ork.music.vt.edu/main/make-your-own-l2ork/software/.

- Jonathan Wilkes' purr-data packages can be found at https://github.com/agraef/purr-data/releases (you can also find packages for OS X/macOS and Windows there).
